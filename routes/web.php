<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Notifications\invoice;
use App\User;
use Carbon\Carbon;
use App\Post;

//Route::get('/', function () {
//    return view('welcome');
//});

//
Route::get('/',function(){
  
   return view('welcome');


});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/addpost', 'HomeController@addpost');

Route::get('/postview','PostController@index');

Route::post('/save',[

'uses'=>'PostController@store',
'as'=>'store'
]);



Route::get('markasread',function(){
	$user=App\User::find(1);
	$user->unreadNotifications->markAsRead();

	return redirect()->back();
})->name('markRead');

// Route::get('/notify',function(){
     
//     $user=Auth::user();
//     $user->notify(new invoice());

// return "ok";

// });


// Route::get('/notify',function(){
     
//    $user=User::get();

//    Notification::send($user,new invoice());


// });


