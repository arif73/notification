@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">

                    @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                  <form action="{{route('store')}}" method="POST">

                      
             {{ csrf_field() }}

                  
                     
                     <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="title">
                      </div>


                      <div class="form-group">
                        <label for="textarea">Textarea</label>
                        <textarea class="form-control" id="textarea" name="textarea" rows="3"></textarea>
                      </div>

                      <button type="submit" class="btn btn-default">Submit</button>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
