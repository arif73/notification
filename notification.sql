-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2018 at 12:27 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notification`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2018_04_18_085443_create_notifications_table', 1),
(16, '2018_04_21_084846_create_posts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('092e2225-34c4-41cf-ab54-cdbfdf129f46', 'App\\Notifications\\invoice', 'App\\User', 1, '{\"data\":\"this is first data\"}', '2018-04-22 04:00:54', '2018-04-22 01:26:40', '2018-04-22 04:00:54'),
('4ecd21d2-bc2e-492c-b74b-a1dccda18ee1', 'App\\Notifications\\invoice', 'App\\User', 1, '{\"data\":\"laravel\"}', '2018-04-22 04:06:10', '2018-04-22 04:04:45', '2018-04-22 04:06:10'),
('67534541-36a3-4db0-9348-043b336afd73', 'App\\Notifications\\invoice', 'App\\User', 1, '{\"data\":\"sssssss\"}', '2018-04-22 04:00:54', '2018-04-22 03:35:22', '2018-04-22 04:00:54'),
('7e901bc3-75a3-478e-8a17-a027119e3c19', 'App\\Notifications\\invoice', 'App\\User', 1, '{\"data\":\"final instantiation\"}', '2018-04-22 04:01:48', '2018-04-22 04:01:44', '2018-04-22 04:01:48'),
('e263d148-22ea-49ad-8f78-a445a1c68b87', 'App\\Notifications\\invoice', 'App\\User', 1, '{\"data\":\"this is notification\"}', '2018-04-22 04:00:54', '2018-04-22 03:18:35', '2018-04-22 04:00:54');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `textarea` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `textarea`, `created_at`, `updated_at`) VALUES
(1, 'dd', 'fdd', '2018-04-22 01:14:34', '2018-04-22 01:14:34'),
(2, 'ddd', 'jyyk', '2018-04-22 01:21:54', '2018-04-22 01:21:54'),
(3, 'first', 'gdg', '2018-04-22 01:33:21', '2018-04-22 01:33:21'),
(4, 'first', 'cd', '2018-04-22 01:40:17', '2018-04-22 01:40:17'),
(5, 'fff', 'ffffffffffff', '2018-04-22 02:00:25', '2018-04-22 02:00:25'),
(6, 'final', 'final post', '2018-04-22 02:17:02', '2018-04-22 02:17:02'),
(7, 'php post', 'laravel framework', '2018-04-22 03:01:33', '2018-04-22 03:01:33'),
(8, 'ddd', 'kkkkk', '2018-04-22 03:08:45', '2018-04-22 03:08:45'),
(9, 'ddd', 'this is notification', '2018-04-22 03:18:35', '2018-04-22 03:18:35'),
(10, 'ddd', 'sssssss', '2018-04-22 03:35:21', '2018-04-22 03:35:21'),
(11, 'ddd', 'ssss', '2018-04-22 03:36:35', '2018-04-22 03:36:35'),
(12, 'final', 'final instantiation', '2018-04-22 04:01:44', '2018-04-22 04:01:44'),
(13, '2nd', 'laravel', '2018-04-22 04:04:45', '2018-04-22 04:04:45'),
(14, 'fff', 'ffffff', '2018-04-22 04:08:31', '2018-04-22 04:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$fdXPTFQ1w2CKUvMrPDcA.OBPxctxFWSsOJTZMNrGoCTyyuH5u8BPS', '5mfrXWpx8uI2GvZZlKBDt4D7OjitqcFfNnJaxACs7r4m1NGWyLwsUKgoZzsL', '2018-04-22 01:14:07', '2018-04-22 01:14:07'),
(2, 'arif', 'arif@gmail.com', '$2y$10$yZ7xv9BpTPfRwG3EB0e8m.a/5aFRpe4xvgSjkizYcJgm4/nWSNnfi', 'OJZM0wIToTZDyqmxIhFRQ1SU7SlaVu1bTb3FXNue5ox2zFS0VW15YpNagnYU', '2018-04-22 01:49:14', '2018-04-22 01:49:14'),
(3, 'jon', 'jon@gmail.com', '$2y$10$ncYfPTn74a4Fn5ZtGqXdJemxdCbre0XUikbNYxXUDGBqCE70VQkci', 'Qe903AYu41HigwQdEdjnNyivfAoIFmCF341enqj4SCNg27THD8BlvYLC1kI8', '2018-04-22 02:18:10', '2018-04-22 02:18:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
